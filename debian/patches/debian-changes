The Debian packaging of taxy-magit-section-el is maintained in git, using the
merging workflow described in dgit-maint-merge(7).  There isn't a patch queue
that can be represented as a quilt series.

A detailed breakdown of the changes is available from their canonical
representation - git commits in the packaging repository.  For example, to see
the changes made by the Debian maintainer in the first upload of upstream
version 1.2.3, you could use:

    % git clone https://git.dgit.debian.org/taxy-magit-section-el
    % cd taxy-magit-section-el
    % git log --oneline 1.2.3..debian/1.2.3-1 -- . ':!debian'

(If you have dgit, use `dgit clone taxy-magit-section-el`, rather than plain
`git clone`.)

A single combined diff, containing all the changes, follows.
--- taxy-magit-section-el-0.12.1.orig/README.org
+++ taxy-magit-section-el-0.12.1/README.org
@@ -1,4 +1,8 @@
 #+TITLE: taxy-magit-section.el
+#+OPTIONS: broken-links:t *:t
+#+TEXINFO_DIR_CATEGORY: Emacs
+#+TEXINFO_DIR_TITLE: Taxy Magit Section: (taxy-magit-section)
+#+TEXINFO_DIR_DESC: Render Taxy structs with Magit Section
 
 #+PROPERTY: LOGGING nil
 
@@ -88,14 +92,8 @@ GPLv3
 
 # Copied from org-super-agenda's readme, in which much was borrowed from Org's =org-manual.org=.
 
-#+OPTIONS: broken-links:t *:t
-
 ** Info export options
 
-#+TEXINFO_DIR_CATEGORY: Emacs
-#+TEXINFO_DIR_TITLE: Taxy Magit Section: (taxy-magit-section)
-#+TEXINFO_DIR_DESC: Render Taxy structs with Magit Section
-
 # NOTE: We could use these, but that causes a pointless error, "org-compile-file: File "..README.info" wasn't produced...", so we just rename the files in the after-save-hook instead.
 # #+TEXINFO_FILENAME: taxy.info
 # #+EXPORT_FILE_NAME: taxy.texi
